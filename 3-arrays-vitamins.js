let items = require("./dataset")
/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

*/

function getAllItems(items, status) {
    return items.filter((item) => item.available === status);
}

console.log(getAllItems(items, true));

function getItemsWithOnlyVitamin(items, vitamin) {
    return items.filter((item) => item.contains === vitamin)
}

console.log(getItemsWithOnlyVitamin(items, "Vitamin C"));


function getItemsWithVitamin(items, vitamin) {
    return items.filter((item) => item.contains.includes(vitamin))
}

console.log(getItemsWithVitamin(items, "Vitamin A"));


function groupItems(items) {
    return items.map((item)=>{
        let output = {}
        output[item.name] = item.contains.split(", ");
        return output;
    }).reduce((acc,curr)=>{
        key = Object.keys(curr);
        if(curr[key[0]].length == 1){
            if (acc[curr[key[0]]] === undefined){
                acc[curr[key[0]]] = [key[0]];
            } else {
                acc[curr[key[0]]].push(key[0]);
            }
        }else{
            if (acc[curr[key[0]][0]] === undefined) {
                acc[curr[key[0]][0]] = [key[0]];
            } else {
                acc[curr[key[0]][0]].push(key[0]);
            }
            if (acc[curr[key[0]][1]] === undefined) {
                acc[curr[key[0]][1]] = [key[0]];
            } else {
                acc[curr[key[0]][1]].push(key[0]);
            }

        }

        return acc;
    },{})
        
}
console.log(groupItems(items));

